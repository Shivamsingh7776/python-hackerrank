if __name__ == '__main__':
    s = input()

    result_string = ''
    occurences = 1
    cur_elem = s[0]

    s = s[1:]

    for elem in s:

        if elem != cur_elem:
            result_string += f'({occurences}, {cur_elem}) '
            cur_elem = elem
            occurences = 0

        occurences += 1

    result_string += f'({occurences}, {cur_elem}) '

    print(result_string.rstrip())
