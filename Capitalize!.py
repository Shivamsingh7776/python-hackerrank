def solve(s):
    new_string = s[0].upper()
    for string in range(1, len(s)):
        if s[string-1] == ' ':
            new_string += s[string].upper()
        else:
            new_string += s[string]
    return new_string

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = solve(s)

    fptr.write(result + '\n')

    fptr.close()
