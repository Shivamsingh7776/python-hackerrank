import numpy


if __name__ == '__main__':
    n, m = map(int, input().split())

    array_matrix = [list(map(int, input().split())) for n in range(m)]

    print(numpy.mean(array_matrix, axis=1))
    print(numpy.var(array_matrix, axis=0))
    std_matrix = (numpy.std(array_matrix))

    final_std = round(std_matrix, 11)
    print(final_std)
