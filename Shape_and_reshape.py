import numpy


def shape_and_reshape(arr):
    return numpy.reshape(arr, (3, 3))


if __name__ == '__main__':

    arr = numpy.array(input().split(), dtype=numpy.int)

    print(shape_and_reshape(arr))
