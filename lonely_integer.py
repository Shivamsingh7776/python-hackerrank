if __name__ == '__main__':
    array_size = int(input())

    array = list(map(int, input().split()))

    array_dict = {}

    for data in array:
        if data not in array_dict:
            array_dict[data] = 1
        else:
            array_dict[data] += 1

    # print(array_dict)
    ans = -1
    for data in array_dict:
        if array_dict[data] == 1:
            ans = data
    print(ans)
