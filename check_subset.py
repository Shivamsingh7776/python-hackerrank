
def check_subset(element_of_a, number_of_b):
    if len(element_of_a.intersection(element_of_b)) == len(element_of_a):
        return True
    else:
        return False


if __name__ == '__main__':
    test_case = int(input())

    while (test_case > 0):
        number_of_a = (input())
        element_of_a = set(map(int, input().split()))

        number_of_b = (input())
        element_of_b = set(map(int, input().split()))

        test_case = test_case - 1

        print(check_subset(element_of_a, number_of_b))
