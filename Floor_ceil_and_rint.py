import numpy


if __name__ == '__main__':
    numpy.set_printoptions(legacy='1.13')
    array = numpy.array(input().split(), float)

    numpy_array = numpy.array(array)

    print(numpy.floor(numpy_array))
    print(numpy.ceil(numpy_array))
    print(numpy.rint(numpy_array))
