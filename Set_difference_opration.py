def set_differnce(subscribed_to_english_newspaper_rollnumber, subscribed_to_english_french_newspaper_rollnumber):
    return len(subscribed_to_english_newspaper_rollnumber.difference(subscribed_to_english_french_newspaper_rollnumber))


if __name__ == '__main__':
    subscribed_to_english_newspaper = int(input())
    subscribed_to_english_newspaper_rollnumber = set(map(int, input().split()))

    subscribed_to_english_french_newspaper = int(input())
    subscribed_to_english_french_newspaper_rollnumber = set(
        map(int, input().split()))

    print(set_differnce(subscribed_to_english_newspaper_rollnumber,
          subscribed_to_english_french_newspaper_rollnumber))
