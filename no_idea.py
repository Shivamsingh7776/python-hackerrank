def no_idea(array, set_1, set_2):
    
    count = 0
    
    for data in set_1:
        if data in array:
            count += 1
    for data in set_2:
        if data in array:
            count -= 1
    return count

if __name__ == '__main__':
    n = input().split()
    array = set(map(int, input().split()))
    
    set_1 = set(map(int, input().split()))
    
    set_2 = set(map(int, input().split()))
    
    print(no_idea(array, set_1, set_2))