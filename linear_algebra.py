import numpy


if __name__ == '__main__':
    n = int(input())
    array_matrix = numpy.array(
        [list(map(float, input().split())) for i in range(n)], dtype=numpy.float)

    print(round(numpy.linalg.det(array_matrix), 2))
