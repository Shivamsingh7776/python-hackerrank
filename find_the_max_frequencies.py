import numpy


def frequencies(array):

    dictionary = {}

    for i in range(len(array)):
        if array[i] not in dictionary:
            dictionary[array[i]] = 1
        else:
            dictionary[array[i]] += 1
    freq = 0

    for i in dictionary.values():
        freq = max(freq, i)
    return freq


if __name__ == '__main__':
    array = numpy.array(input().split(), int)
    # array = [1, 1, 2, 2, 3, 3, 3, 3]

    print(frequencies(array))
