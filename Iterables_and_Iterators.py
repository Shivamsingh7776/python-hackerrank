from itertools import combinations

if __name__ == '__main__':
    n = int(input())
    n_list = input().split()
    k = int(input())
    x = 0
    y = 0

    for i in combinations(n_list, k):
        if 'a' in i:
            x += 1
        y += 1

    print(x / y)
