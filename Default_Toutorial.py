from collections import defaultdict

if __name__ == '__main__':
    dictA = defaultdict(list)
    listb = []

    numa, numb = map(int, input().split())
    for i in range(1, numa + 1):
        dictA[input()].append(i)

    for _ in range(numb):
        listb.append(input())

    for s in listb:
        if s in dictA:
            print(" ".join(map(str, dictA[s])))
        else:
            print("-1")
