def any_or_all(array, array_data):
    x = True
    y = False
    for num in array:
        if num < 0:
            x = False
            break
        if str(num) == str(num)[::-1]:
            y = True

    return y and x


if __name__ == "__main__":
    total_number_of_int = int(input())

    array = list(map(int, input().split()))

    array_data = array[::-1]
    #

    print(any_or_all(array, array_data))
