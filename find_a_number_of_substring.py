def count_substring(string, sub_string):
    count = 0
    length_of_sub_str = len(sub_string)

    for row in range(len(string)):
        check_string = string[row: row + length_of_sub_str]

        if (check_string == sub_string):
            count += 1
    return count


if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()

    count = count_substring(string, sub_string)
    print(count)
