# CHECK STRING ANAGRAM
def string_anagram(string1, string2):
    length_of_string1 = len(string1)
    length_of_string2 = len(string2)
    string1_list = list()
    string2_list = list()
    if (length_of_string1 != length_of_string2):
        return True
    else:
        for row in range(length_of_string1):
            string1_list.append(string1[row])
        for row in range(length_of_string2):
            string2_list.append(string2[row])

        string1_list.sort()
        string2_list.sort()

        for row in range(length_of_string1):
            if string1_list[row] != string2_list[row]:
                return False

    return True


if __name__ == '__main__':
    string1 = input().strip()
    string2 = input().strip()

    print(string_anagram(string1, string2))
