def swap_case(s):
    list_data = list()
    string = ""
    for row in range(len(s)):
        if (s[row].isupper()):
            list_data.append(s[row].lower())
        elif (s[row].islower()):
            list_data.append(s[row].upper())
        elif (s[row].isdigit or s[row] == ' ' or s[row] == '.'):
            list_data.append(s[row])

    for row in list_data:
        string += row
    return string


if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)
