from collections import deque

if __name__ == '__main__':
    number_of_oprations = int(input())
    list_data = deque()
    for num_ops in range(number_of_oprations):
        method_name, values = ((int, input().split()))

        list_data1 = deque((method_name, values))
        list_data3 = (list_data1[1])

        if list_data3[0] == 'append':
            list_data.append(list_data3[1])
        elif list_data3[0] == 'appendleft':
            list_data.appendleft(list_data3[1])
        elif list_data3[0] == 'pop':
            list_data.pop()
        elif list_data3[0] == 'popleft':
            list_data.popleft()
    for data in list_data:
        print(data, end=" ")
