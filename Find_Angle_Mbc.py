import math

if __name__ == '__main__':
    a_b = int(input())
    b_c = int(input())

    a_c = math.sqrt(a_b*a_b + b_c*b_c)
    # print(a_c)
    m_c = a_c/2

    angle = math.degrees(math.asin(m_c/b_c))

    print(angle)
