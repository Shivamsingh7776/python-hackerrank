import numpy


def arrays(arr):
    # complete this function
    # use numpy.array
    reverser_numpy = numpy.array(arr, float)

    return numpy.flip(reverser_numpy)


arr = input().strip().split(' ')
result = arrays(arr)
print(result)
