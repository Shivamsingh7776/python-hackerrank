import numpy


def concateate(array1, array2):
    return numpy.concatenate((array1, array2), axis=0)


if __name__ == '__main__':
    n, m, p = map(int, input().split())

    array1 = [list(map(int, input().split())) for p in range(n)]
    array2 = [list(map(int, input().split())) for p in range(m)]

    print(concateate(array1, array2))
