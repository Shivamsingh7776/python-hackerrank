
if __name__ == '__main__':
    test_cases = int(input())

    for test_case in range(test_cases):
        uid = input()

        dictinary = {}
        if (len(uid) != 10):
            print('Invalid')
            break

        for data in range(len(uid)):
            if uid[data] not in dictinary:
                dictinary[uid[data]] = 1
            else:
                dictinary[uid[data]] += 1

        for data in dictinary.values():
            if data > 1:
                print('Invalid')
                break
        # print(dictinary)

        count_digit = 0
        cout_uppercase_char = 0

        for data in range(len(uid)):
            if uid[data] >= '0' and uid[data] <= '9':
                count_digit += 1
            elif uid[data] >= 'A' and uid[data] <= 'B':
                cout_uppercase_char += 1

        if count_digit < 3 and cout_uppercase_char < 2:
            print('Invalid')
            break
        else:
            print('Valid')
            break
