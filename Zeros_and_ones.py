import numpy


if __name__ == '__main__':
    input_data = list(map(int, input().split()))
    print(input_data)
    print(numpy.zeros(input_data, dtype=numpy.int))
    print(numpy.ones(input_data, dtype=numpy.int))
