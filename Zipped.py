if __name__ == '__main__':
    n, x = map(int, input().split())
    y = x
    list_data = list()

    while (x > 0):
        array_marks = list(map(float, input().strip().split()))
        list_data.append(array_marks)
        x -= 1
    zip_data = list(zip(*list_data))

    for data in range(n):
        print(round(sum(zip_data[data])/y, 1))
