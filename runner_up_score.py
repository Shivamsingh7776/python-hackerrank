def runner_up_score(arr, n):
    arr.remove(max(arr))
    return max(arr)
    
if __name__ == '__main__':
    n = int(input())
    arr = set(map(int, input().split()))
    
    print(runner_up_score(arr, n))
    