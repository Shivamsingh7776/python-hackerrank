def symmetric_difference(m_data, n_data):
    x = m_data.difference(n_data)
    y = n_data.difference(m_data)

    m = list(x)
    n = list(y)

    ans = []

    for i in range(len(m)):
        ans.append(m[i])

    for i in range(len(n)):
        ans.append(n[i])

    ans.sort()
    for i in range(len(ans)):
        print(ans[i])


if __name__ == '__main__':
    M = int(input())
    m_data = set(map(int, input().split()))

    N = int(input())
    n_data = set(map(int, input().split()))

    (symmetric_difference(m_data, n_data))
