def print_formatted(number):
    # your code goes here
    w = len(str(bin(number))[2:])
    for num in range(1, number + 1):
        print(str(num).rjust(w, ' '), str(oct(num)[2:]).rjust(w, ' '), str(
            hex(num)[2:]).upper().rjust(w, ' '), str(bin(num)[2:]).rjust(w, ' '))


if __name__ == '__main__':
    n = int(input())
    print_formatted(n)
