import numpy


def min_and_max(numpy_array):
    numpy_min_array = numpy.min(numpy_array, axis=1)

    return max(numpy_min_array)


if __name__ == '__main__':
    n, m = map(int, input().split())

    array = [list(map(int, input().split())) for m in range(n)]

    numpy_array = numpy.array(array)

    print(min_and_max(numpy_array))
