def intersection_opration(english_data, french_data):
    return len(english_data.intersection(french_data))


if __name__ == '__main__':
    number_of_student_subscribed_to_english_newspaper = int(input())
    english_data = set(map(int, input().split()))
    number_of_student_subscribed_to_french_newspaper = int(input())
    french_data = set(map(int, input().split()))

    print(intersection_opration(english_data, french_data))
