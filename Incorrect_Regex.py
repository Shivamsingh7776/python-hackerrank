# Enter your code here. Read input from STDIN. Print output to STDOUT
import re
if __name__ == '__main__':

    # Take Input
    inputs = [input() for _ in range(int(input()))]

    # Use try except to check if valid
    for input in inputs:
        try:
            re.compile(input)
            print('True')
        except:
            print('False')
