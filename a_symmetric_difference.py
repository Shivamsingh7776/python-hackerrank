
def a_symmetric_differnce(student_roll_number_english_newspaper, student_roll_number_french_newspaper):
    return len(student_roll_number_english_newspaper.symmetric_difference(student_roll_number_french_newspaper))


if __name__ == "__main__":
    number_of_student_who_subscribe_english_newspaper = int(input())
    student_roll_number_english_newspaper = set(map(int, input().split()))

    number_of_student_who_subscribe_french_newspaper = int(input())
    student_roll_number_french_newspaper = set(map(int, input().split()))

    print(a_symmetric_differnce(student_roll_number_english_newspaper,
          student_roll_number_french_newspaper))
