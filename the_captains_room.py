from collections import Counter

if __name__ == '__main__':
    K = int(input())
    n = list(map(int, input().split()))
    c = Counter(n)
    print([key for key, count in c.items() if count == 1][0])
