# Enter your code here. Read input from STDIN. Print output to STDOUT

if __name__ == '__main__':
    string = str(input())

    list_data_low = []
    list_data_upper = []
    list_data_digit1 = []
    list_data_digit2 = []

    for data in range(len(string)):
        if string[data].islower():
            list_data_low.append(string[data])
        elif string[data].isupper():
            list_data_upper.append(string[data])
        elif string[data].isdigit():

            if int(string[data]) % 2 != 0:

                list_data_digit1.append(int(string[data]))
            else:
                list_data_digit2.append(int(string[data]))

    list_data_digit1.sort()
    list_data_digit2.sort()
    list_data_low.sort()
    list_data_upper.sort()
    list_final = []

    for element in list_data_low:
        list_final.append(element)
    for element in list_data_upper:
        list_final.append(element)
    for element in list_data_digit1:

        list_final.append(str(element))
    for element in list_data_digit2:
        list_final.append(str(element))

    string_final = ""
    for element in list_final:
        string_final += element

    print(string_final)
