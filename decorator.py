def logger(func):
    def inner(*args, **kwargs):
        print ("Arguments were: %s, %s" % (args, kwargs))
        return func(*args, **kwargs) #2
    return inner
    
@logger
def fool(x, y=1):
    return x * y
@logger   
def foo2():
     return 2
     

@logger
def shivam(x, y, z):
    return x + y + z
     
print(fool(5))

print(foo2())

print(foo2())

print(shivam(2, 3, 5))