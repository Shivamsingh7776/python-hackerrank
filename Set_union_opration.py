def number_of_student_who_have_atleast_one_subscription(english_data, french_data):
    return len(english_data.union(french_data))


if __name__ == '__main__':
    number_of_student_subscribed_to_english_newspaper = int(input())
    english_data = set(map(int, input().split()))
    number_of_student_subscribed_to_french_newspaper = int(input())
    french_data = set(map(int, input().split()))

    print(number_of_student_who_have_atleast_one_subscription(
        english_data, french_data))
