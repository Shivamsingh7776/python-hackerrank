if __name__ == '__main__':
    length_of_array = int(input())

    array_list = list(map(int, input().split()))

    diffs = []
    array_list.sort()
    for data in range(len(array_list)-1):
        diffs.append(abs(array_list[data]-array_list[data+1]))
    print(min(diffs))
