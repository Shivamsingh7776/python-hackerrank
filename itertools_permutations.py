from itertools import permutations

if __name__ == '__main__':
    string, k = input().split()

    ans_string = ""

    list_data = list(string)

    list_data_comes = (list(permutations((list_data), int(k))))

    for data in range(len(list_data_comes)):
        ans_string += str(list_data_comes[data])

    print(ans_string)
