def list_comprehension(x, y, z, n):
    answer = []
    
    for row in range(x + 1):
        for col in range(y + 1):
            for data in range(z + 1):
                if(row + col + data != n):
                    answer.append([row, col, data])
    return answer
if __name__ == '__main__':
    x = int(input())
    y = int(input())
    z = int(input())
    n = int(input())
    
    print(list_comprehension(x, y, z, n))