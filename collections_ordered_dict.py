from collections import OrderedDict

if __name__ == '__main__':
    number_of_items = int(input())
    dict_data = OrderedDict()
    for num in range(number_of_items):
        val = input().split()
        name = ' '.join(val[:-1])
        price = int(val[-1])

        if name not in dict_data:
            dict_data[name] = price
        else:
            dict_data[name] += price
    # print(dict_data)

    for data in dict_data.items():
        print(*data)
