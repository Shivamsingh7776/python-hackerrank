#!/bin/python3

import math
import os
import random
import re
import sys


if __name__ == '__main__':
    nm = input().split()

    n = int(nm[0])

    m = int(nm[1])

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    k = int(input())

    final_list = list(zip(*arr))
    final_list[k] = list(final_list[k])
    final_list[k].sort()
    L = []
    for i in final_list[k]:
        for j in arr:
            if i == j[k]:
                if j not in L:
                    L.append(j)
                    print(*j)
