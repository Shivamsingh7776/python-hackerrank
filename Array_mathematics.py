import numpy

if __name__ == '__main__':
    n, m = map(int , input().split())
    
    array1 = [list(map(int, input().split())) for row in range(n)]
    array2 = [list(map(int, input().split())) for row in range(n)]
    
    print(numpy.add(array1, array2))
    print(numpy.subtract(array1, array2))
    print(numpy.multiply(array1, array2))
    print(numpy.floor_divide(array1, array2))
    print(numpy.mod(array1, array2))
    print(numpy.power(array1, array2))
    