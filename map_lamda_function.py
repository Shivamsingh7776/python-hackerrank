cube = lambda x: x**3# complete the lambda function 

def fibonacci(n):
        # return a list of fibonacci numbers
        n1 = 0
        n2 = 1
        fib = [n1,n2]
        if n == 0:
                return []
        elif n == 1:
                return [n1]
        elif n == 2:
                return fib
        else:
                for i in range(n-2):
                        n3 = n1+n2
                        fib.append(n3)
                        n1 = n2
                        n2 = n3
                return fib

    

if __name__ == '__main__':
    n = int(input())
    print(list(map(cube, fibonacci(n))))