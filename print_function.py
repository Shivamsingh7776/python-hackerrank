def print_function(numbers):
    for number in range(1, numbers+1):
        print(number, end="")
    
if __name__ == '__main__':
    n = int(input())
    print_function(n)