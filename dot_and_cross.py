import numpy


def dot_and_cross(array1, array2):
    return numpy.dot(array1, array2)


if __name__ == '__main__':
    n = int(input())

    array1 = numpy.array([list(map(int, input().split()))
                         for data in range(n)])
    array2 = numpy.array([list(map(int, input().split()))
                         for data in range(n)])

    print(dot_and_cross(array1, array2))
