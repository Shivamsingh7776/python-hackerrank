import re


if __name__ == '__main__':

    pattern = re.compile(r'([a-zA-Z0-9])\1')
    mat = pattern.search(input())
    if mat:
        print(mat.group(1))
    else:
        print(-1)
