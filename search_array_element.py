def search_target_element(arr, number, target):

    for num in range(number):
        if (arr[num] == target):
            return num


if __name__ == '__main__':
    number = int(input())
    target = int(input())
    arr = list(map(int, input().split()))

    print(search_target_element(arr, number, target))
