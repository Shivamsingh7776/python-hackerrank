import re


if __name__ == '__main__':
    numbers = [input() for i in range(int(input()))]

    # regex
    regex = re.compile(r"^(7|8|9)([0-9]){9}$")

    # Check
    for number in numbers:
        if regex.match(number):
            print('YES')
        else:
            print('NO')