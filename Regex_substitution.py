import re


def subst(match):
    # print(match.group(0))
    if match.group(0) == '&& ':
        return 'and '
    if match.group(0) == '|| ':
        return 'or '
    raise ValueError


n = int(input())
for _ in range(n):
    line = input()
    print(re.sub(r'((?<= )\&\& )|((?<= )\|\| )', subst, line))
