import numpy as np

def array_transpose(array):
    return np.transpose(array)
    
def array_flatten(array):
    return np.array(array).flatten()

if __name__ == '__main__':
    row, col = map(int, input().split())

    array = [list(map(int, input().split())) for col in range(row)]

    print(array_transpose(array))
    print(array_flatten(array))