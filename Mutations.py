def mutate_string(string, position, character):

    list_data = list(string)
    list_data[position] = character
    string = ''.join(list_data)

    return string


if __name__ == '__main__':
    s = input()
    i, c = input().split()
    s_new = mutate_string(s, int(i), c)
    print(s_new)
