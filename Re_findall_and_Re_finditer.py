import re

if __name__ == '__main__':
    s = str(input())
    s = s.strip("AEIOUaeiou")
    result = re.findall(r"[AEIOUaeiou]{2,}", s)
    if not result:
        print(-1)
    else:
        [print(r) for r in result]
