
import math
import os
import random
import re
import sys


def solve(meal_cost, tip_percent, tax_percent):
    # Write your code here
    tip = (meal_cost*tip_percent)/100
    tax = (tax_percent*meal_cost)/100

    total_cost = tip + tax + meal_cost

    print(round(total_cost))


if __name__ == '__main__':
    meal_cost = float(input().strip())

    tip_percent = int(input().strip())

    tax_percent = int(input().strip())

    solve(meal_cost, tip_percent, tax_percent)
