import numpy


if __name__ == '__main__':
    p = list(map(float, input().split()))
    x = int(input())

    print(numpy.polyval(p, x))
