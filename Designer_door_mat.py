# Enter your code here. Read input from STDIN. Print output to STDOUT

if __name__ == '__main__':
    n, m = map(int, input().split())
    a, b = ".|.", "WELCOME"
    k = -1
    for i in range(n//2):
        k += 2
        print((k*a).center(m, "-"))
    print(b.center(m, "-"))
    for i in range(n//2):
        print((a*k).center(m, "-"))
        k -= 2
