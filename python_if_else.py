#!/bin/python3

import math
import os
import random
import re
import sys

def python_if_else(n):
    if(n % 2 == 0):
        if(n >= 2 and n <= 5):
            print('Not Weird')
        elif(n >= 6 and n <= 20):
            print('Weird')
        else:
            print('Not Weird')
    else:
        print('Weird')

if __name__ == '__main__':
    n = int(input().strip())
    
    python_if_else(n)
    
            
