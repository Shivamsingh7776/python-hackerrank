def list_data(N):
    list_data = list()

    for data in range(N):
        command = input().split()

        if command[0] == 'insert':
            list_data.insert(int(command[1]), int(command[2]))
        elif command[0] == 'sort':
            list_data.sort()
        elif command[0] == 'reverse':
            list_data.reverse()
        elif command[0] == 'pop':
            list_data.pop()
        elif command[0] == 'append':
            list_data.append(int(command[1]))

        elif command[0] == 'remove':
            list_data.remove(int(command[1]))
        elif command[0] == 'print':
            print(list_data)


if __name__ == '__main__':
    N = int(input())

    list_data(N)
