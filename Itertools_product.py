from itertools import product

if __name__ == '__main__':
    A, B = list(map(int, input().split())), list(map(int, input().split()))
    print(*product(A, B))
