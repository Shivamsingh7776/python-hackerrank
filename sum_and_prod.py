import numpy


def sum_and_product(numpy_array):
    numpy_sum_array = numpy.sum(numpy_array, axis=0)

    return numpy.product(numpy_sum_array)


if __name__ == '__main__':
    n, m = map(int, input().split())

    array = [list(map(int, input().split())) for m in range(n)]

    numpy_array = numpy.array(array)

    print(sum_and_product(numpy_array))
