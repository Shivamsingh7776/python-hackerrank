import re


def is_valid_credit_card(number):
    # check if it starts with 4, 5 or 6
    if not re.match(r'^[4-6]', number):
        return False
    # check if it contains exactly 16 digits
    if not re.match(r'^\d{16}$|^(\d{4}-){3}\d{4}$', number):
        return False
    # check if it only contains digits
    if not re.match(r'^\d+$', number.replace("-", "")):
        return False
    # check if it does not have 4 or more consecutive repeated digits
    if re.search(r'(\d)\1{3,}', number.replace("-", "")):
        return False
    return True


if __name__ == '__main__':
    n = int(input())
    credit_cards = []

    for i in range(n):
        credit_cards.append(input())

    for i in range(n):
        if is_valid_credit_card(credit_cards[i]):
            print("Valid")
        else:
            print("Invalid")
