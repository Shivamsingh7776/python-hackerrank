def minion_game(string):
    # your code goes here
    stuart_strings = []
    kevin_strings = []
    st_tot = 0
    k_tot = 0
    vowels = ['A', 'E', 'I', 'O', 'U']
    for data in range(len(string)):
        if string[data] in vowels:
            k_tot += len(string)-data
        else:
            st_tot += len(string)-data
    if k_tot > st_tot:
        print("Kevin {}".format(k_tot))
    elif k_tot < st_tot:
        print("Stuart {}".format(st_tot))
    else:
        return print("Draw")


if __name__ == '__main__':
    s = input()
    minion_game(s)
