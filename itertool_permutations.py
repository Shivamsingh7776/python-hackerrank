# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import permutations


def permutation(word, size):
    word = list(word.upper())
    size = int(size)
    
    possible_permutations = sorted(list(permutations(word, size)))
    for data in possible_permutations :
        print(''.join(list(data)))

if __name__ == '__main__':
    word, size = input().split()

    permutation(word, size)
    
    
    
