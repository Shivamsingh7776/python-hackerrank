def exception_occur(a, b):
    try:
        print(int(a)//int(b))

    except ZeroDivisionError as e:
        print("Error Code: integer division or modulo by zero")
    except ValueError as v:
        print("Error Code:", v)


if __name__ == '__main__':
    t = int(input())
    while t > 0:
        a, b = input().split()
        t = t-1

        exception_occur(a, b)
