if __name__ == '__main__':
    dic = {}
    s = list()
    for _ in range(int(input())):
        name = input()
        score = float(input())
        
        if score in dic:
            dic[score].append(name)
        else:
            dic[score] = [name]
        
        if score not in s:
            s.append(score)
            
    min1 = min(s)
    s.remove(min1)
        
    min2 = min(s)
    dic[min2].sort()
    for row in dic[min2]:
        print(row)
            