import re


if __name__ == '__main__':
    # Text
    text = input()

    # Regex
    regex = re.compile(input())

    # Search
    matches = regex.search(text)

    # Until we get matches print them else print -1
    if matches:
        while matches:
            print((matches.start(), matches.end() - 1))
            matches = regex.search(text, matches.start() + 1)
    else:
        print((-1, -1))
