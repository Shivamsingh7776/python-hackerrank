from collections import namedtuple

if __name__ == '__main__':
    N = int(input())
    columns = namedtuple('student', input())
    marks_sum = 0

    for n in range(N):
        marks_sum += int(columns(*input().split()).MARKS)

    print(round(marks_sum / N, 2))
