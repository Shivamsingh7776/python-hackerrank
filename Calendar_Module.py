# Enter your code here. Read input from STDIN. Print output to STDOUT
import calendar
import datetime


if __name__ == '__main__':
    m, d, y = input().split()
    days = ["MONDAY", "TUESDAY", "WEDNESDAY",
            "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"]
    c = calendar.weekday(int(y), int(m.strip("0")), int(d.strip("0")))
    print((days[c]))
