def split_and_join(line):
    # write your code here
    list_data = list()
    list_data_final = list()
    string = ""

    for row in range(len(line)):
        list_data.append(line[row])

    for row in range(len(list_data)):
        if (list_data[row] == ' '):
            list_data_final.append('-')
        else:
            list_data_final.append(list_data[row])

    for row in range(len(list_data_final)):
        string += list_data_final[row]

    return string


if __name__ == '__main__':
    line = input()
    result = split_and_join(line)
    print(result)
